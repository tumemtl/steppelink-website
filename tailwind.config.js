/** @type {import('tailwindcss').Config} */
module.exports = {
    mode: "jit",
    content: ["./src/**/*.{js,jsx,ts,tsx}", "./components/**/*.{js,ts,jsx,tsx}"],

    important: true,


    theme: {
        extend: {
            colors: {
                theme: "#000F29",
                light: "#F2F2F2",
                darkBlue: "#000F26",
                grayDark: "#292929",
                black: "#000000",
                white: "#ffffff",
                blue: "#075AC6",
                orange: "#E75700",
                gray: "#1B1C20",
                grayLight: "#666F7F",
                orangeDark: "#D14F00",
                orangeDarkDark: "#B14300",
            },

            fontFamily: {
                main: ["Montserrat", "sans-serif"],
                roboto: ["Roboto", "sans-serif"],
            },
            fontSize: {
                xs: ".75rem",
                sm: ".875rem",
                tiny: ".875rem",
                base: "1rem",
                lg: "1.125rem",
                xl: "1.25rem",
                "2xl": "1.5rem",
                "3xl": "1.875rem",
                "4xl": "2.25rem",
                "5xl": "3rem",
                "6xl": "4rem",
                "7xl": "5rem",
            },
            lineHeight: {
                mobile: "30px",
                12: "3rem",
            },
            transitionProperty: {
                width: "width",
            },
        },
    },

    plugins: [],
};