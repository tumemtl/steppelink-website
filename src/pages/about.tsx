import React from "react";
import "../styles/global.css";

import Header from "../components/Navbars/Header";
import Timeline from "../components/Timeline";
import Footer from "../components/Footer";
import ContactUsBtn from "../components/Buttons/ContactUsBtn";


export default function about() {
  return (
    <>
      <Header />
      <div className="px-[5%] pt-[100px] text-white md:px-[10%]">
        <Timeline />
        <ContactUsBtn />
        <Footer />
      </div>
    </>
  );
}
