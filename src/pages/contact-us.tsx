import React from "react";
import "../styles/global.css";



import Header from "../components/Navbars/Header";


export default function contactUs() {
  return (
    <>
      <Header />

      <div className="px-[5%] pt-[100px] text-white md:px-[10%]">
        <h2 className="my-[4rem] text-center md:text-left">Contact us</h2>
        <div className="mb-[4rem] gap-[2rem] md:grid md:w-[75%] md:grid-cols-2">
          <input
            className="mb-[2rem] h-[2rem] w-full bg-orangeDarkDark pl-[1rem] placeholder:text-white placeholder:opacity-30 md:mb-0"
            type="text"
            placeholder="First Name"
          />
          <input
            className="mb-[2rem] h-[2rem] w-full bg-orangeDarkDark pl-[1rem] placeholder:text-white placeholder:opacity-30 md:mb-0"
            type="text"
            placeholder="Last Name"
          />
          <input
            className="mb-[2rem] h-[2rem] w-full bg-orangeDarkDark pl-[1rem] placeholder:text-white placeholder:opacity-30 md:mb-0"
            type="text"
            placeholder="Email"
          />
          <input
            className="mb-[2rem] h-[2rem] w-full bg-orangeDarkDark pl-[1rem] placeholder:text-white placeholder:opacity-30 md:mb-0"
            type="text"
            placeholder="Phone"
          />
          <input
            className="mb-[2rem] h-[2rem] w-full bg-orangeDarkDark pl-[1rem] placeholder:text-white placeholder:opacity-30 md:mb-0"
            type="text"
            placeholder="Company name"
          />
          <select className="mb-[2rem] h-[2rem] w-full bg-orangeDarkDark pl-[0.8rem] placeholder:text-white placeholder:opacity-30 focus:outline-none md:mb-0">
            <option value="" className="opacity-30">
              What service are you interested in
            </option>
            <option value="">Neg</option>
            <option value="">Hoyr</option>
            <option value="">Gurav</option>
            <option value="">Duruv</option>
            <option value="">Tav</option>
            <option value="">Zurgaa</option>
            <option value="">Doloo</option>
          </select>
        </div>
        <div>
          <textarea
            name="Additional information"
            id=""
            cols={30}
            rows={7}
            placeholder="Additional information"
            className="w-full bg-orangeDarkDark p-[1rem] text-white placeholder:text-white placeholder:opacity-30 md:w-full"
          ></textarea>
        </div>
        <div className="my-[4rem] items-center text-center md:flex md:text-left">
          <p>
            O
            <span className="text-[14px]">
              R CONTACT US DIRECTLY AT
              <span className="font-[700]"> SALES@STEPPELINK.MN</span>
            </span>
          </p>
          <button className="mt-[2rem] h-[4rem] bg-white px-[5rem] text-[24px] font-[600] text-orange md:mt-0 md:ml-[4rem]">
            S<span className="text-[20px]">END</span>
          </button>
        </div>
      </div>

    </>
  );
}
