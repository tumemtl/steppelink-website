import React from "react";
import Layout from "../../components/Layout";
import { StaticImage } from "gatsby-plugin-image";

export default function blogSlug() {
  return (
    <Layout title="blog">
      <div className="font-roboto">
        <section className="flex justify-between">
          <div className="max-w-[510px]">
            <h4 className="text-[35px] font-[700]">
              Your portfolio is stopping you from geting that job
            </h4>
            <StaticImage src="../../images/blogs/blog2.png" alt="" />
            <p className="my-9 text-[15px] font-[300] leading-[22.5px]">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc
              vulputate libero et velit interdum, ac aliquet odio mattis. Class
              aptent taciti sociosqu ad litora torquent per conubia nostra, per
              inceptos himenaeos. Curabitur tempus urna at turpis condimentum
              lobortis. Ut commodo efficitur neque. Ut diam quam, semper iaculis
              condimentum ac, vestibulum eu nisl. Lorem ipsum dolor sit amet,
              consectetur adipiscing elit. Nunc vulputate libero et velit
              interdum, ac aliquet odio mattis. Class aptent taciti sociosqu ad
              litora torquent per conubia nostra, per inceptos himenaeos.
              Curabitur tempus urna at turpis condimentum lobortis. Ut commodo
              efficitur neque. Ut diam quam, semper iaculis condimentum ac,
              vestibulum eu nisl.
            </p>
            <div className="h-[1px] max-w-[610px] bg-grayLight "></div>
          </div>
        </section>
      </div>
    </Layout>
  );
}
