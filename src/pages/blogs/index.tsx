import React from "react";
import "../../styles/global.css";

import CartContainer from "../../components/blog/CartContainer";
import { StaticImage } from "gatsby-plugin-image";
import Layout from "../../components/Layout";

export default function blogs() {
  return (
    <>
      <Layout title="Blog">
        <div className="flex justify-between">
          <div className="md:basis-[60%]">
            <CartContainer />
          </div>
          <div className=" hidden pl-[15%] md:block md:basis-[40%]">
            <section>
              <form className="flex items-center">
                <label className="sr-only">Search</label>
                <div className="relative w-full">
                  <div className="pointer-events-none absolute inset-y-0 left-0 flex items-center pl-3">
                    <svg
                      className=" h-5 w-5 text-[#e6e6e6]"
                      fill="currentColor"
                      viewBox="0 0 20 20"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        fill-rule="evenodd"
                        d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
                        clip-rule="evenodd"
                      ></path>
                    </svg>
                  </div>
                  <input
                    type="text"
                    id="simple-search"
                    className="text-gray-900 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 block w-full rounded-full border border-[#e6e6e6] bg-transparent p-2 pl-10 text-sm dark:text-white"
                    placeholder="Search"
                    required
                  />
                </div>
              </form>
            </section>
            <section className="pl-1">
              <div className="mt-5 mb-2 flex items-center">
                <div className="h-2 w-2 rounded-full bg-orange"></div>
                <h4 className="text ml-2 text-[15px] font-[500] leading-[17px]">
                  {" "}
                  What We’re Reading Today
                </h4>
              </div>
              <div className="[&>*]:py-2">
                <section>
                  <div className="flex items-center  py-2 align-middle [&>*]:mr-[10px]">
                    <StaticImage
                      src="../../images/about/profile.png"
                      width={20}
                      alt=""
                    />
                    <p className="text-[12px] font-[300]">Ami Baas</p>
                  </div>
                  <h4 className="py-0 text-[15px] font-[400] leading-[17.5px]">
                    Your portfolio is stopping you from geting that job
                  </h4>
                </section>
                <section>
                  <div className="flex items-center  py-2 align-middle [&>*]:mr-[10px]">
                    <StaticImage
                      src="../../images/about/profile.png"
                      width={20}
                      alt=""
                    />
                    <p className="text-[12px] font-[300]">Ami Baas</p>
                  </div>
                  <h4 className="py-0 text-[15px] font-[400] leading-[17.5px]">
                    Your portfolio is stopping you from geting that job
                  </h4>
                </section>
                <section>
                  <div className="flex items-center  py-2 align-middle [&>*]:mr-[10px]">
                    <StaticImage
                      src="../../images/about/profile.png"
                      width={20}
                      alt=""
                    />
                    <p className="text-[12px] font-[300]">Ami Baas</p>
                  </div>
                  <h4 className="py-0 text-[15px] font-[400] leading-[17.5px]">
                    Your portfolio is stopping you from geting that job
                  </h4>
                </section>
                <div>
                  <p className="text-[12px] font-[300] leading-[] text-orange">
                    See full list
                  </p>
                </div>
              </div>
              <div>
                <h4 className="my-5 text-[15px] font-[400] leading-4">
                  Recommended Topic
                </h4>
                <div className="flex flex-wrap [&>*]:mr-2 [&>*]:mb-2">
                  <button className="rounded-full bg-light px-3 py-2 text-[13px] font-[400] text-grayDark">
                    Technology
                  </button>

                  <button className="rounded-full bg-light px-3 py-2 text-[13px] font-[400] text-grayDark">
                    Sex
                  </button>
                  <button className="rounded-full bg-light px-3 py-2 text-[13px] font-[400] text-grayDark">
                    Drugs
                  </button>
                  <button className="rounded-full bg-light px-3 py-2 text-[13px] font-[400] text-grayDark">
                    Money
                  </button>
                  <button className="rounded-full bg-light px-3 py-2 text-[13px] font-[400] text-grayDark">
                    Money
                  </button>
                </div>
              </div>
            </section>
          </div>
        </div>
      </Layout>
    </>
  );
}
