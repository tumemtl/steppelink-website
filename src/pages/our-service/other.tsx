import * as React from "react";
import ServiceNav from "../../components/Navbars/ServiceNav";
import Header from "../../components/Navbars/Header";
import { StaticImage } from "gatsby-plugin-image";
import OtherCart from "../../components/OurServices/OtherCart";
import ContactUsBtn from "../../components/Buttons/ContactUsBtn";

export default function Other() {
  return (
    <>
      <Header />
      <div className="px-[5%] pt-[162px] text-white md:px-[10%]">
        <ServiceNav />

        <div>
          <div className="grid grid-cols-2 pt-[4rem]">
            <div className="z-0 opacity-50 md:basis-[70%] md:opacity-100">
              <StaticImage
                className="-left-[5%]"
                // width={1000}
                placeholder="none"
                src="../../images/services/Laptop.png"
                alt=""
              />
            </div>

            {/* Element for grid setting */}
            {/* <div></div> */}

            <div className="w-[80%] md:pl-[4rem]">
              <h2 className="pt-[3rem] pb-[1rem]">
                Other
                <span className="text-orange"> development</span>
              </h2>
              <p>
                We will help define exactly what your organization wants through
                its website. It can also be a structured web application with a
                fully dynamic Content Management System (CMS) that can be used
                by anyone with any level of IT knowledge.
              </p>
            </div>
          </div>

          <OtherCart />
        </div>
        <ContactUsBtn />
      </div>
    </>
  );
}
