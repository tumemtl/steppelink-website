import * as React from "react";

import ServiceNav from "../../components/Navbars/ServiceNav";
import Header from "../../components/Navbars/Header";
import { StaticImage } from "gatsby-plugin-image";
import SystemDevCart from "../../components/OurServices/SystemDevCart";
import ContactUsBtn from "../../components/Buttons/ContactUsBtn";


interface SystemDEvProps {}

const SystemDEv: React.FC<SystemDEvProps> = () => {
  return (
    <>
      <Header />
      <div className="px-[5%] pt-[162px] text-white md:px-[10%]">
        <ServiceNav />
        <div className="px flex-row gap-5 px-[5%] pt-[100px] md:flex ">
          <section className="basis-[55%]">
            <StaticImage
              width={500}
              src="../../images/services/erp.png"
              alt="mobile"
            />
          </section>
          <section className="basis-[45%]">
            <h2 className="leading-[67px]">
              System <br />
              <span className="text-orange">development</span>
            </h2>
            <p className="text-[17px] leading-[20px]">
              We will help define exactly what your organization wants through
              its website. It can also be a structured web application with a
              fully dynamic Content Management System (CMS) that can be used by
              anyone with any level of IT knowledge.
            </p>
          </section>
        </div>
        <div className="grid grid-cols-1 gap-4 pt-[100px] md:grid-cols-2">
          <SystemDevCart text="text" title="title" />
          <SystemDevCart text="text" title="title" />
          <SystemDevCart text="text" title="title" />
          <SystemDevCart text="text" title="title" />
        </div>
        <ContactUsBtn />
      </div>
    </>
  );
};

export default SystemDEv;
