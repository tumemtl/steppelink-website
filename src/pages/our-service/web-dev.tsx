import React from "react";
import "../../styles/global.css";
import { StaticImage } from "gatsby-plugin-image";


import Header from "../../components/Navbars/Header";
import ServiceNav from "../../components/Navbars/ServiceNav";
import WebNav from "../../components/Navbars/WebNav";
import HeroBtn from "../../components/Buttons/HeroBtn";
import ContactUsBtn from "../../components/Buttons/ContactUsBtn";


export default function ourService() {
  return (
    <>
      <Header />

      <div className="px-[5%] pt-[162px] text-white md:px-[10%]">
        <ServiceNav />
        <div className="pt-[4rem] md:grid md:grid-cols-2 md:gap-[2rem] md:p-[4rem]">
          <div className="relative">
            <StaticImage
              className=""
              placeholder="none"
              src="../../images/services/web.png"
              alt=""
            />
          </div>
          <div className="">
            <h2 className="text-center md:text-left">
              Web <br />
              <span className="text-orange">Development</span>
            </h2>
            <p className="mt-[2rem] text-[16px]">
              We will help define exactly what your organization wants through
              its website. It can also be a structured web application with a
              fully dynamic Content Management System (CMS) that can be used by
              anyone with any level of IT knowledge.
            </p>
          </div>
        </div>
        <div className="md:flex">
          <WebNav />
          <div className="md:ml-[50px]">
            <StaticImage
              width={800}
              className="md:absolute md:z-20 md:translate-y-[30px] md:translate-x-[30px]"
              src="../../images/web/mesa.png"
              alt=""
            />
            <StaticImage
              className="relative z-0 hidden md:block"
              src="../../images/web/bg.png"
              alt=""
            />
          </div>
        </div>
        <div className="mt-[4rem] text-center">
          <HeroBtn text="Contact Us" onClick={console.log} />
        </div>
        <ContactUsBtn />
      </div>
    </>
  );
}
