import React from "react";
import "../../styles/global.css";
import { StaticImage } from "gatsby-plugin-image";


import Header from "../../components/Navbars/Header";
import ServiceNav from "../../components/Navbars/ServiceNav";
import MobileCart from "../../components/Carts/MobileCart";
import ContactUsBtn from "../../components/Buttons/ContactUsBtn";


export default function ourService() {
  return (
    <>
      <Header />


      <div className=" px-[5%] pt-[162px] text-white md:px-[10%]">

        <ServiceNav />

        <div className="mid:p-[4rem] relative z-0 mb-[4rem] flex flex-col md:grid md:grid-cols-2 md:gap-[2rem]">
          <div className="absolute z-0 opacity-50 md:basis-[70%] md:opacity-100">
            <StaticImage
              className="-left-[20%] md:-left-[25%]"
              // width={1000}
              placeholder="none"
              src="../../images/services/Mobile.png"
              alt=""
            />
            {/* <div className="z-0"> */}
            <StaticImage
              placeholder="none"
              className="absolute md:top-[15rem]"
              src="../../images/services/ellipse-right.png"
              alt=""
            />
            {/* </div> */}
          </div>

          {/* Spacing between Mobile image and its description */}
          <div className="basis-[55%]"></div>

          <div className="relative z-20 basis-[45%] pt-[8rem]">

            <h2 className="text-center text-[40px] leading-[3rem] md:text-left md:leading-[4rem] lg:text-[55px]">
              Mobile <br />
              <div className="text-orange">Development</div>
            </h2>
            <p className="mt-[2rem] text-[16px]">
              We will help define exactly what your organization wants through
              its website. It can also be a structured web application with a
              fully dynamic Content Management System (CMS) that can be used by
              anyone with any level of IT knowledge.
            </p>
          </div>
        </div>

        <MobileCart />

        <div className="mb-[5rem] h-[25rem] w-full border-b-[2px] border-white bg-orange opacity-50"></div>

        <div className="mb-[4rem]">
          <h2 className="text-center">
            WHAT OUR{" "}
            <span className="text-orange">
              MOBILE <br /> TEAM
            </span>{" "}
            HAS MADE
          </h2>
        </div>

        <div className="mb-[4rem] text-center md:align-middle">
          <StaticImage
            placeholder="none"
            width={100}
            className="m-[0.6rem] md:mx-[1.7rem] md:align-middle"
            src="../../images/logos/Steppe-arena-1.png"
            alt="Steppe Arena"
          />
          <StaticImage
            placeholder="none"
            width={100}
            className="m-[0.6rem] md:mx-[1.7rem] md:align-middle"
            src="../../images/logos/esan-1.png"
            alt="Esan"
          />
          <StaticImage
            placeholder="none"
            width={100}
            className="m-[0.6rem] md:mx-[1.7rem] md:align-middle"
            src="../../images/logos/Mesa-2.png"
            alt="MESA"
          />
          <StaticImage
            placeholder="none"
            width={80}
            className="m-[0.6rem] md:mx-[1.7rem] md:align-middle"
            src="../../images/logos/MSC-1.png"
            alt="Mongolian Sports Corporation"
          />
          <StaticImage
            placeholder="none"
            width={90}
            className="m-[0.6rem] md:mx-[1.7rem] md:align-middle"
            src="../../images/logos/Odin-1.png"
            alt="ODIN"
          />
          <StaticImage
            placeholder="none"
            // width={90}
            className="m-[0.6rem] md:mx-[1.7rem] md:align-middle"
            src="../../images/logos/Ember.png"
            alt="Ember"
          />
        </div>
        <div className="mx-auto mb-[4rem] text-center">
          <StaticImage
            placeholder="none"
            width={100}
            className="m-[0.6rem] md:mx-[1.7rem] md:align-middle"
            src="../../images/logos/Steppe-arena-1.png"
            alt="Steppe Arena"
          />
          <StaticImage
            placeholder="none"
            width={100}
            className="m-[0.6rem] md:mx-[1.7rem] md:align-middle"
            src="../../images/logos/esan-1.png"
            alt="Esan"
          />
          <StaticImage
            placeholder="none"
            width={100}
            className="m-[0.6rem] md:mx-[1.7rem] md:align-middle"
            src="../../images/logos/Mesa-2.png"
            alt="MESA"
          />
          <StaticImage
            placeholder="none"
            width={80}
            className="m-[0.6rem] md:mx-[1.7rem] md:align-middle"
            src="../../images/logos/MSC-1.png"
            alt="Mongolian Sports Corporation"
          />
          <StaticImage
            placeholder="none"
            width={90}
            className="m-[0.6rem] md:mx-[1.7rem] md:align-middle"
            src="../../images/logos/Odin-1.png"
            alt="ODIN"
          />
          <StaticImage
            placeholder="none"
            // width={90}
            className="m-[0.6rem] md:mx-[1.7rem] md:align-middle"
            src="../../images/logos/Ember.png"
            alt="Ember"
          />
        </div>

        <div className="mb-[4rem] text-center md:align-middle">
          <StaticImage
            placeholder="none"
            width={100}
            className="m-[0.6rem] md:mx-[1.7rem] md:align-middle"
            src="../../images/logos/Steppe-arena-1.png"
            alt="Steppe Arena"
          />
          <StaticImage
            placeholder="none"
            width={100}
            className="m-[0.6rem] md:mx-[1.7rem] md:align-middle"
            src="../../images/logos/esan-1.png"
            alt="Esan"
          />
          <StaticImage
            placeholder="none"
            width={100}
            className="m-[0.6rem] md:mx-[1.7rem] md:align-middle"
            src="../../images/logos/Mesa-2.png"
            alt="MESA"
          />
          <StaticImage
            placeholder="none"
            width={80}
            className="m-[0.6rem] md:mx-[1.7rem] md:align-middle"
            src="../../images/logos/MSC-1.png"
            alt="Mongolian Sports Corporation"
          />
          <StaticImage
            placeholder="none"
            width={90}
            className="m-[0.6rem] md:mx-[1.7rem] md:align-middle"
            src="../../images/logos/Odin-1.png"
            alt="ODIN"
          />
          <StaticImage
            placeholder="none"
            // width={90}
            className="m-[0.6rem] md:mx-[1.7rem] md:align-middle"
            src="../../images/logos/Ember.png"
            alt="Ember"
          />
        </div>

        <div className="mb-[4rem] text-center md:align-middle">
          <StaticImage
            placeholder="none"
            width={100}
            className="m-[0.6rem] md:mx-[1.7rem] md:align-middle"
            src="../../images/logos/Steppe-arena-1.png"
            alt="Steppe Arena"
          />
          <StaticImage
            placeholder="none"
            width={100}
            className="m-[0.6rem] md:mx-[1.7rem] md:align-middle"
            src="../../images/logos/esan-1.png"
            alt="Esan"
          />
          <StaticImage
            placeholder="none"
            width={100}
            className="m-[0.6rem] md:mx-[1.7rem] md:align-middle"
            src="../../images/logos/Mesa-2.png"
            alt="MESA"
          />
          <StaticImage
            placeholder="none"
            width={80}
            className="m-[0.6rem] md:mx-[1.7rem] md:align-middle"
            src="../../images/logos/MSC-1.png"
            alt="Mongolian Sports Corporation"
          />
          <StaticImage
            placeholder="none"
            width={90}
            className="m-[0.6rem] md:mx-[1.7rem] md:align-middle"
            src="../../images/logos/Odin-1.png"
            alt="ODIN"
          />
          <StaticImage
            placeholder="none"
            // width={90}
            className="m-[0.6rem] md:mx-[1.7rem] md:align-middle"
            src="../../images/logos/Ember.png"
            alt="Ember"
          />
        </div>

        <ContactUsBtn />
      </div>
    </>
  );
}
