import React from "react";
import "../../styles/global.css";

import { FaGithub, FaLinkedin, FaUserCircle } from "react-icons/fa";

import Header from "../../components/Navbars/Header";
import ApplyBtn from "../../components/Buttons/HeroBtn2";
import ShareBtn from "../../components/Buttons/ShareBtn";

export default function Apply() {
  return (
    <>
      <Header />
      <div className="px-[5%] pt-[100px] text-white md:px-[10%]">
        <h2 className="text-center md:text-left">Full stack developer</h2>

        <div className="my-[2rem] flex justify-between">
          <p>LS Plaza Office</p>
          <div className="flex items-center justify-center">
            <ShareBtn />
            <ApplyBtn />
          </div>
        </div>

        <div className="w-full border-l-[10px] border-orange bg-[#AFB2B7] py-[1.5rem] pl-[1.5rem]">
          <p className="text-black">
            Applications will be accepted through June 26, 2022.
          </p>
        </div>

        <div className="mt-[4rem] w-full">
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu
            turpis molestie, dictum est a, mattis tellus. Sed dignissim, metus
            nec fringilla accumsan, risus sem sollicitudin lacus, ut interdum
            tellus elit sed risus. Maecenas eget condimentum velit, sit amet
            feugiat lectus. Class aptent taciti sociosqu ad litora torquent per
            conubia nostra, per inceptos himenaeos. Praesent auctor purus luctus
            enim egestas, ac scelerisque ante pulvinar. Donec ut rhoncus ex.
            Suspendisse ac rhoncus nisl, eu tempor urna. Curabitur vel bibendum
            lorem. Morbi convallis convallis diam sit amet lacinia. Aliquam in
            elementum tellus. <br /> &emsp; Curabitur tempor quis eros tempus
            lacinia. Nam bibendum pellentesque quam a convallis. Sed ut
            vulputate nisi. Integer in felis sed leo vestibulum venenatis.
            Suspendisse quis arcu sem. Aenean feugiat ex eu vestibulum
            vestibulum. Morbi a eleifend magna. Nam metus lacus, porttitor eu
            mauris a, blandit ultrices nibh. Mauris sit amet magna non ligula
            vestibulum eleifend. Nulla varius volutpat turpis sed lacinia. Nam
            eget mi in purus lobortis eleifend. Sed nec ante dictum sem
            condimentum ullamcorper quis venenat
          </p>
        </div>

        <div className="my-[4rem] px-[4rem]">
          <div className="items-center justify-center md:flex">
            <h2 className="w-full text-center text-orange md:py-[3rem] md:text-left md:text-[100px]">
              JOIN US
            </h2>
            <div className="flex w-full flex-col">
              <input
                className="my-[0.5rem] h-[2rem] bg-orangeDarkDark pl-[1rem] placeholder:text-white placeholder:opacity-30"
                type="text"
                placeholder="First Name"
              />
              <input
                className="my-[0.5rem] h-[2rem] bg-orangeDarkDark pl-[1rem] placeholder:text-white placeholder:opacity-30"
                type="text"
                placeholder="Last Name"
              />
              <input
                className="my-[0.5rem] h-[2rem] bg-orangeDarkDark pl-[1rem] placeholder:text-white placeholder:opacity-30"
                type="text"
                placeholder="Phone Number"
              />
            </div>
          </div>
          <div className="my-[4rem] items-center justify-between bg-orange py-[0.5rem] px-[2rem] md:flex">
            <h3 className="text-[24px] font-[700]">RESUME</h3>
            <p className="md:w:-[40%]">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit.
            </p>
            <div className="text-[12px]">
              <button className="h-[30px] bg-[#D9D9D9] px-[4rem] text-black">
                Upload
              </button>
              <p className="pt-[10px] text-left">File size: 2MB | PDF or Doc</p>
            </div>
          </div>

          <div className=" my-[1rem] flex justify-between">
            <span className="flex items-center justify-center">
              <FaGithub className="w-[3rem]" size={24} />
              <p>Github (Optional)</p>
            </span>
            <input
              className="h-[2rem] w-[70%] bg-orangeDarkDark pl-[1rem] placeholder:text-white placeholder:opacity-30"
              type="text"
              placeholder="Type here"
            />
          </div>
          <div className=" my-[1rem] flex justify-between">
            <span className="flex items-center justify-center">
              <FaLinkedin className="w-[3rem]" size={24} />
              <p>LinkedIn (Optional)</p>
            </span>
            <input
              className="h-[2rem] w-[70%] bg-orangeDarkDark pl-[1rem] placeholder:text-white placeholder:opacity-30"
              type="text"
              placeholder="Type here"
            />
          </div>
          <div className=" my-[1rem] flex justify-between">
            <span className="flex items-center justify-center">
              <FaUserCircle className="w-[3rem]" size={24} />
              <p>Portfolio (Opitonal)</p>
            </span>
            <input
              className="h-[2rem] w-[70%] bg-orangeDarkDark pl-[1rem] placeholder:text-white placeholder:opacity-30"
              type="text"
              placeholder="Type here"
            />
          </div>
        </div>
        <div>
          <button className="mb-[4rem] w-full bg-white py-[2rem] text-[24px] font-[500] text-orange">
            Submit
          </button>
        </div>
      </div>
    </>
  );
}
