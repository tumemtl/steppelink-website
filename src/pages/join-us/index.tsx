import { Link } from "gatsby";
import { StaticImage } from "gatsby-plugin-image";
import React from "react";
import Abilities from "../../components/Abilities";
import Header from "../../components/Navbars/Header";
import ApplyBtn from "../../components/Buttons/HeroBtn2";
import ShareBtn from "../../components/Buttons/ShareBtn";
import ContactUsBtn from "../../components/Buttons/ContactUsBtn";
export default function JoinUs() {
  return (
    <div>
      <Header />
      <div className="px-[5%] pt-[100px] text-white md:px-[10%]">
        <div className="my-[4rem] md:w-[70%]">
          <h2 className="mb-[2rem]">
            Behind every great project is a
            <span className="text-orange"> great team</span>
          </h2>
          <p>
            We rely heavily on our Steppelink LLC’s Steppe Payment. In the past
            we had difficult control over our customers’ transactions, but now
            our admin is able to access every information related from one
            place.
          </p>
        </div>

        <Abilities />
      </div>
      <div className="h-full gap-[2rem] bg-gray p-[4rem] px-[5%] text-center text-white md:grid md:grid-cols-2 md:px-[10%]">
        <div className="mb-[2rem] max-w-max md:mb-0">
          <StaticImage
            className="w-full"
            src="../../images/join-us/office.png"
            alt=""
          />
          <p className="my-[1rem] text-left">
            LS Plaza Office <br />
            09:00-18:00
            <br />
            Full Stack Developer
          </p>
          <p className="my-[1rem] text-left">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc
            vulputate libero et velit interdum, ac aliquet odio mattis. Class
            aptent taciti sociosqu ad litora torquent per conubia nostra, per
            inceptos himenaeos. Curabitur tempus urna at turpis condimentum
            lobortis. Ut commodo efficitur neque. Ut diam quam, semper iaculis
            condimentum ac, vestibulum eu nisl. Lorem ipsum dolor sit amet,
            consectetur adipiscing elit. Nunc vulputate libero et velit
            interdum, ac aliquet odio mattis. Class aptent taciti sociosqu ad
            litora torquent per conubia nostra, per inceptos himenaeos.
            Curabitur tempus urna at turpis condimentum lobortis. Ut commodo
            efficitur neque. Ut diam quam, semper iaculis condimentum ac,
            vestibulum eu nisl.
          </p>
          <div className="flex justify-end">
            <ShareBtn />
            <a href="/join-us/apply">
              <ApplyBtn />
            </a>
          </div>
        </div>
        <div className="max-w-max">
          <StaticImage
            className="w-full"
            src="../../images/join-us/office2.png"
            alt=""
          />
          <p className="my-[1rem] text-left">
            LS Plaza Office <br />
            09:00-18:00
            <br />
            Full Stack Developer
          </p>
          <p className="my-[1rem] text-left">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc
            vulputate libero et velit interdum, ac aliquet odio mattis. Class
            aptent taciti sociosqu ad litora torquent per conubia nostra, per
            inceptos himenaeos. Curabitur tempus urna at turpis condimentum
            lobortis. Ut commodo efficitur neque. Ut diam quam, semper iaculis
            condimentum ac, vestibulum eu nisl. Lorem ipsum dolor sit amet,
            consectetur adipiscing elit. Nunc vulputate libero et velit
            interdum, ac aliquet odio mattis. Class aptent taciti sociosqu ad
            litora torquent per conubia nostra, per inceptos himenaeos.
            Curabitur tempus urna at turpis condimentum lobortis. Ut commodo
            efficitur neque. Ut diam quam, semper iaculis condimentum ac,
            vestibulum eu nisl.
          </p>
          <div className="flex justify-end">
            <ShareBtn />
            <a href="/join-us/apply">
              <ApplyBtn />
            </a>
          </div>
        </div>
      </div>
      <div className="px-[5%] pt-[100px] text-white md:px-[10%]">
        <ContactUsBtn />
      </div>
    </div>
  );
}
