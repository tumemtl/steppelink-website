import React from "react";
import "../styles/global.css";

import Header from "../components/Navbars/Header";
import CartContainer from "../components/blog/CartContainer";

export default function blogs() {
  return (
    <>
      <Header />
      <div className="px-[5%] pt-[162px] text-white md:px-[10%]">
        <CartContainer />
      </div>
    </>
  );
}
