import * as React from "react";
import "../styles/global.css";
import Header from "../components/Navbars/Header";

import Imagination from "../components/Imagination";
import Technology from "../components/Technology";
import Cart from "../components/Carts/Cart";
import Projects from "../components/Projects";
import Team from "../components/Team";
import Footer from "../components/Footer";


const IndexPage = () => {
  return (
    <div className="text-white">
      <Header />
      <div className=" px-[5%] pt-[100] md:px-[10%]">
        <Imagination />
        <Cart />
        <Technology />
        <Projects />
        <Projects />
        <Team />
        <Footer />
      </div>
    </div>
  );
};

export default IndexPage;
