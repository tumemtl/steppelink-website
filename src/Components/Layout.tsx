import * as React from "react";
import Header from "./Navbars/Header";

type Props = {
  children: React.ReactNode;
  title: string;
};

const Layout = ({ children, title = "Title here......" }: Props) => {
  return (
    <div className="text-white">
      <Header />
      <div className=" px-[5%] pt-[162px] md:px-[10%]">{children}</div>
    </div>
  );
};

export default Layout;
