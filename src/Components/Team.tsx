import React from "react";

import HeroBtn from "./Buttons/HeroBtn";

import { StaticImage } from "gatsby-plugin-image";

export default function Content() {
  return (
    <div className=" text-center ">
      <h2 className="mx-auto mb-[5%] text-center text-[40px] leading-[3rem] md:w-[70%] md:text-[55px] md:leading-[4rem]">
        Behind every great project is a
        <span className="text-orange"> great team </span>
      </h2>
      <p className="mx-auto mb-[4rem] text-[18px] font-[200] md:w-[80%] md:text-[25px]">
        {" "}
        We rely heavily on our Steppelink LLC’s Steppe Payment. In the past we
        had difficult control over our customers’ transactions, but now our
        admin is able to access every information related from one place.
      </p>
      <div className="mb-[4rem]">
        <HeroBtn text="Join Us" onClick={console.log} />
      </div>
      {/* <div className="pt-[2rem]  text-center">
        <HeroBtn text="Join Us" onClick={console.log} />
      </div> */}
      <div className="md:flex md:justify-between">
        <StaticImage
          width={350}
          className=" mb-[3rem] md:mb-0 md:align-middle"
          src="../images/team/Rectangle-7.png"
          alt="Steppe Arena"
        />
        <StaticImage
          width={350}
          className=" mb-[3rem] md:mb-0 md:align-middle"
          src="../images/team/Rectangle-8.png"
          alt="Steppe Arena"
        />
        <StaticImage
          width={350}
          className=" mb-[3rem] md:mb-0 md:align-middle"
          src="../images/team/Rectangle-9.png"
          alt="Steppe Arena"
        />
      </div>
    </div>
  );
}
