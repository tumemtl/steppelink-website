import React from "react";

export default function Abilities() {
  return (
    <div className="pt-[4rem]">
      <div className="mt-[]">
        <h2 className="mb-[4rem] text-center md:text-left">
          What makes us{" "}
          <span className="py-[10px] text-center text-orange ">good</span>
        </h2>

        <div className="mb-[2rem] flex">
          <p className="my-auto mr-[1rem] w-[35%] rounded-tr-full rounded-br-full bg-orange py-[10px] pl-[0.5rem] text-[18px] md:w-[30%] md:px-[2rem] md:text-center md:text-[24px]">
            Agility
          </p>
          <p className="my-auto flex justify-items-center text-[16px] md:pl-[30%] md:text-[24px]">
            WE ARE AGILE AND STUFF
          </p>
        </div>

        <div className="mb-[2rem] flex">
          <p className="my-auto mr-[1rem] w-[40%] rounded-tr-full rounded-br-full bg-orange py-[10px] pl-[0.5rem] text-[18px] md:w-[50%] md:px-[2rem] md:text-center md:text-[24px]">
            Awesomeness
          </p>
          <p className="my-auto text-[16px] md:pl-[15%] md:text-[24px]">
            WE ARE AGILE AND STUFF
          </p>
        </div>

        <div className="mb-[2rem] flex">
          <p className="my-auto mr-[1rem] w-[35%] rounded-tr-full rounded-br-full bg-orange py-[10px] pl-[0.5rem] text-[18px] md:w-[30%] md:px-[2rem] md:text-center md:text-[24px]">
            Friendliness
          </p>
          <p className="my-auto text-[16px] md:pl-[30%] md:text-[24px]">
            WE ARE AGILE AND STUFF
          </p>
        </div>
      </div>
    </div>
  );
}
