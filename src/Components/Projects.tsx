import * as React from "react";
import ProjectCart from "./Carts/ProjectCart";

const Projects = () => {
  return (
    <div>
      <h2 className="mb-[40px]">Projects</h2>
      <div className="grid grid-cols-2 gap-4 md:grid-cols-4">
        <ProjectCart />
        <ProjectCart />
        <ProjectCart />
        <ProjectCart />
      </div>
      <div className="mt-4 flex justify-end">
        <button className="hover:text-greyDark duration-50 relative h-[2.3rem] w-[2rem] rounded-tr-lg rounded-bl-lg border-2 border-solid border-orange text-[1.2rem] font-[100] ease-out hover:bg-orange hover:ease-in active:border-orangeDarkDark">
          &#60;
        </button>
        <button className="hover:text-greyDark duration-50 relative ml-4 h-[2.3rem] w-[2rem] rounded-tl-lg rounded-br-lg border-2 border-solid border-orange text-[1.2rem] font-[100] ease-out hover:bg-orange hover:ease-in active:border-orangeDarkDark">
          &#62;
        </button>
      </div>
    </div>
  );
};

export default Projects;
