import * as React from "react";

import { StaticImage } from "gatsby-plugin-image";
interface CartProps {
  // text: string;
  // translateValue: number;
  // imageUrl: string;
}

const Cart: React.FunctionComponent<CartProps> = (props) => {
  return (
    <div className="mt-[100px] flex  grid-cols-2 flex-col gap-5 md:grid">
      {/* // -----1------ */}
      <div className="mx-auto h-[613px] max-w-[550px] overflow-hidden bg-gray text-white ">
        <div className=" mx-[5%]">
          <div className="!relative">
            <StaticImage
              src="../../images/services/ellipse.png"
              alt="ellipse"
            />
            <div className="">
              <StaticImage
                className="!absolute z-0 -translate-y-[80%]"
                src="../../images/services/erp.png"
                placeholder="tracedSVG"
                alt="ERP"
              />
            </div>
          </div>
          <div>
            <div className=" !relative z-10 mt-[95px] !block h-[300px] text-[16px] ">
              <h3 className=" pb-[20px] text-[21px]">System Development</h3>
              <p className="font-400 text-[16px] leading-[19.5px]">
                We will help define exactly what your organization wants through
                its website. It can also be a structured web application with a
                fully dynamic Content Management System (CMS) that can be used
                by anyone with any level of IT knowledge.
              </p>
              <div>
                <button className="!absolute left-0  mt-[60px] items-end rounded-tl-lg rounded-br-lg border-[3px] border-solid border-orange bg-gray   px-[23px]">
                  Learn more
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* // -----2------ */}
      <div className="mx-auto h-[613px] max-w-[550px] overflow-hidden bg-gray text-white ">
        <div className=" mx-[5%]">
          <div className="!relative">
            <StaticImage
              src="../../images/services/ellipse.png"
              alt="ellipse"
            />
            <div>
              <StaticImage
                // className={`!absolute z-0 --translate-y-[-${props.translateValue}%]`}
                // src={`${props.imageUrl}`}
                className=" !absolute z-0 -translate-y-[60%]"
                width={500}
                height={400}
                src="../../images/services/mobile.png"
                placeholder="tracedSVG"
                alt="ERP"
              />
            </div>
          </div>
          <div>
            <div className=" !relative z-10 mt-[95px] block h-[300px] text-[16px] ">
              <h3 className=" pb-[20px] text-[21px]">System Development</h3>
              <p className="font-400 text-[16px] leading-[19.5px]">
                We will help define exactly what your organization wants through
                its website. It can also be a structured web application with a
                fully dynamic Content Management System (CMS) that can be used
                by anyone with any level of IT knowledge.
              </p>
              <div>
                <button className="!absolute left-0 bottom-[] mt-[60px] items-end rounded-tl-lg rounded-br-lg border-[3px] border-solid border-orange bg-gray   px-[23px]">
                  Learn more
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* // -----3------ */}
      <div className="mx-auto h-[613px] max-w-[550px] overflow-hidden bg-gray text-white ">
        <div className=" mx-[5%]">
          <div className="!relative">
            <StaticImage
              src="../../images/services/ellipse.png"
              alt="ellipse"
            />
            <div>
              <StaticImage
                // className={`!absolute z-0 --translate-y-[-${props.translateValue}%]`}
                // src={`${props.imageUrl}`}
                className="!absolute z-0 -translate-y-[60%]"
                src="../../images/services/web.png"
                placeholder="tracedSVG"
                alt="ERP"
              />
            </div>
          </div>
          <div>
            <div className=" !relative z-10 mt-[95px] block h-[300px] text-[16px] ">
              <h3 className=" pb-[20px] text-[21px]">System Development</h3>
              <p className="font-400 text-[16px] leading-[19.5px]">
                We will help define exactly what your organization wants through
                its website. It can also be a structured web application with a
                fully dynamic Content Management System (CMS) that can be used
                by anyone with any level of IT knowledge.
              </p>
              <div>
                <button className="!absolute left-0  mt-[60px] items-end rounded-tl-lg rounded-br-lg  border-[3px] border-solid border-orange bg-gray   px-[23px]">
                  Learn more
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* // -----4------ */}
      <div className="mx-auto h-[613px] max-w-[550px] overflow-hidden bg-gray text-white ">
        <div className=" mx-[5%]">
          <div className="!relative">
            <StaticImage
              src="../../images/services/ellipse.png"
              alt="ellipse"
            />
            <div className="">
              <StaticImage
                // className={`!absolute z-0 --translate-y-[-${props.translateValue}%]`}
                // src={`${props.imageUrl}`}
                className="!absolute z-0 -translate-y-[50%]"
                src="../../images/services/ai.png"
                placeholder="tracedSVG"
                alt="ERP"
              />
            </div>
          </div>
          <div>
            <div className=" !relative z-10 mt-[95px] block h-[300px] text-[16px] ">
              <h3 className=" pb-[20px] text-[21px]">System Development</h3>
              <p className="font-400 text-[16px] leading-[19.5px]">
                We will help define exactly what your organization wants through
                its website. It can also be a structured web application with a
                fully dynamic Content Management System (CMS) that can be used
                by anyone with any level of IT knowledge.
              </p>
              <div>
                <button className="!absolute left-0  mt-[60px] items-end rounded-tl-lg rounded-br-lg  border-[3px] border-solid border-orange bg-gray   px-[23px]">
                  Learn more
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* // -----5------ */}
      <div className="col-span-2  mx-auto max-h-[613px] overflow-hidden bg-gray text-white ">
        <div className="">
          <div className="grid grid-cols-2 gap-4">
            <div className="!relative mx-[5%] max-w-[550px]">
              <StaticImage
                src="../../images/services/ellipse.png"
                alt="ellipse"
              />
              <div className="">
                <StaticImage
                  // className={`!absolute z-0 --translate-y-[-${props.translateValue}%]`}
                  // src={`${props.imageUrl}`}
                  className="!absolute z-0 -translate-y-[60%]"
                  src="../../images/services/web.png"
                  placeholder="tracedSVG"
                  alt="ERP"
                />
              </div>
            </div>
            <div className=" !relative z-10 mx-[5%] mt-[95px] block h-[300px] text-[16px]">
              <h3 className=" pb-[20px] text-[21px]">System Development</h3>
              <p className="font-400 text-[16px] leading-[19.5px]">
                We will help define exactly what your organization wants through
                its website. It can also be a structured web application with a
                fully dynamic Content Management System (CMS) that can be used
                by anyone with any level of IT knowledge.
              </p>
              <div>
                <button className="!absolute left-0  mt-[60px] items-end rounded-tl-lg rounded-br-lg  border-[3px] border-solid border-orange bg-gray   px-[23px]">
                  Learn more
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Cart;

{
  /* <div className=" bg-white --translate-y-[-50%] mx-[5%]">
  <div className=" bg-orange flex items-center w-full aspect-square rounded-full  m-auto ">
    <div className="w-[55%] bg-gray aspect-square rounded-full m-auto  items-center"></div>
  </div>
 <div/> */
}
