import * as React from "react";
import { StaticImage } from "gatsby-plugin-image";

const ProjectCart = () => {
  return (
    <div>
      <div className="relative min-h-[300px] max-w-[270px] rounded-r-lg border-l-[3px] border-orange  bg-gray">
        <div className=" min-h-[18]">
          <div className="">
            {/* Image */}
            <div className="flex justify-evenly">
              <StaticImage src="../images/projects/pic-1.png" alt="pic" />
            </div>
            <div className="p-[7%]">
              <h1 className="text-[1rem] font-[700]  leading-[1.1rem]">
                Steppe Arena
              </h1>
              <p className="pt-[0.7rem] text-[10.7px]">
                Developed for all types of smartphones (iPhone, Android,
                Windows), mobile devic es (iOS, android, windows) for easy
                access to information and payments.
              </p>
            </div>
          </div>

          <div className="-mt-3">
            <button className="hover:text-greyDark duration-50 absolute bottom-0 right-0 mb-0 mr-0 ml-4 h-[2.2rem]  w-[1.8rem] rounded-tl-lg rounded-br-lg border-2 border-solid border-orange bg-orange text-[1.2rem] font-[100] ease-out hover:bg-gray hover:ease-in active:border-orangeDarkDark">
              &#62;
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProjectCart;
