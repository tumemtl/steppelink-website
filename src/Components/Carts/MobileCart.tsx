import React from "react";
import { StaticImage } from "gatsby-plugin-image";

export default function Content() {
  return (
    <div className="md:mt-[15rem] relative z-20 md:grid md:grid-cols-2 md:p-[4rem] md:gap-[2rem]">
      <span className="md:mt-[4rem]">
        <div className="bg-orange relative h-[40rem] mb-[2rem] md:mb-[2rem] w-full rounded-md border-[2px]">
          <div className="pr-[5rem] p-[2rem]">
            <p>Meest</p>
            <p className="font-[600]">
              A service for order and delivery of goods from foreign online
              stores
            </p>
          </div>
          <div className="absolute  bottom-0 left-[50%] -translate-x-[50%] w-[90%]">
            <StaticImage
              placeholder="none"
              width={1500}
              className="absolute bottom-0"
              src="../images/services/Mobile2.png"
              alt=""
            />
          </div>
        </div>
        <div className="bg-theme relative h-[40rem] mb-[2rem] md:mb-0 w-full rounded-md border-[2px]">
          <div className="pr-[5rem] p-[2rem]">
            <p>Meest</p>
            <p className="font-[600]">
              A service for order and delivery of goods from foreign online
              stores
            </p>
          </div>
          <div className="absolute bottom-0 left-[50%] -translate-x-[50%] w-[90%]">
            <StaticImage
              placeholder="none"
              width={1500}
              className="absolute bottom-0"
              src="../images/services/Mobile2.png"
              alt=""
            />
          </div>
        </div>
      </span>

      <span>
        <div className="bg-theme relative h-[40rem] mb-[2rem] md:mb-[2rem] w-full rounded-md border-[2px]">
          <div className="pr-[5rem] p-[2rem]">
            <p>Meest</p>
            <p className="font-[600]">
              A service for order and delivery of goods from foreign online
              stores
            </p>
          </div>
          <div className="absolute bottom-0 left-[50%] -translate-x-[50%] w-[90%]">
            <StaticImage
              placeholder="none"
              width={1500}
              className="absolute bottom-0"
              src="../images/services/Mobile2.png"
              alt=""
            />
          </div>
        </div>
        <div className="bg-orange relative h-[40rem] mb-[2rem] md:mb-0 w-full rounded-md border-[2px]">
          <div className="pr-[5rem] p-[2rem]">
            <p>Meest</p>
            <p className="font-[600]">
              A service for order and delivery of goods from foreign online
              stores
            </p>
          </div>
          <div className="absolute bottom-0 left-[50%] -translate-x-[50%] w-[90%]">
            <StaticImage
              placeholder="none"
              width={1500}
              className="absolute bottom-0"
              src="../images/services/Mobile2.png"
              alt=""
            />
          </div>
        </div>
      </span>
    </div>
  );
}
