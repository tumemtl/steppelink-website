import { StaticImage } from "gatsby-plugin-image";
import React from "react";

export default function Teammates() {
  return (
    <div>
      <div className="mb-[4rem]">
        <h2 className="md:text-left text-center mb-[2rem]">Our Team</h2>
        <div className="flex flex-wrap justify-center mx-auto">
          <div className="inline-block lg:mx-[2rem] mx-[1rem] mb-[3rem]">
            <StaticImage width={150} src="../images/about/profile.png" alt="" />
            <div className="mt-[2rem] text-center">
              <span>
                <p className="text-[] font-[600]">Turbold</p>
                <p className="font-[200] text-[16px] text-grayLight">
                  Project Manager
                </p>
              </span>
            </div>
          </div>
          <div className="inline-block lg:mx-[2rem] mx-[1rem] mb-[3rem]">
            <StaticImage width={150} src="../images/about/profile.png" alt="" />
            <div className="mt-[2rem] text-center">
              <span>
                <p className="text-[] font-[600]">Turbold</p>
                <p className="font-[200] text-[16px] text-grayLight">
                  Project Manager
                </p>
              </span>
            </div>
          </div>
          <div className="inline-block lg:mx-[2rem] mx-[1rem] mb-[3rem]">
            <StaticImage width={150} src="../images/about/profile.png" alt="" />
            <div className="mt-[2rem] text-center">
              <span>
                <p className="text-[] font-[600]">Turbold</p>
                <p className="font-[200] text-[16px] text-grayLight">
                  Project Manager
                </p>
              </span>
            </div>
          </div>
          <div className="inline-block lg:mx-[2rem] mx-[1rem] mb-[3rem]">
            <StaticImage width={150} src="../images/about/profile.png" alt="" />
            <div className="mt-[2rem] text-center">
              <span>
                <p className="text-[] font-[600]">Turbold</p>
                <p className="font-[200] text-[16px] text-grayLight">
                  Project Manager
                </p>
              </span>
            </div>
          </div>
          <div className="inline-block lg:mx-[2rem] mx-[1rem] mb-[3rem]">
            <StaticImage width={150} src="../images/about/profile.png" alt="" />
            <div className="mt-[2rem] text-center">
              <span>
                <p className="text-[] font-[600]">Turbold</p>
                <p className="font-[200] text-[16px] text-grayLight">
                  Project Manager
                </p>
              </span>
            </div>
          </div>
          <div className="inline-block lg:mx-[2rem] mx-[1rem] mb-[3rem]">
            <StaticImage width={150} src="../images/about/profile.png" alt="" />
            <div className="mt-[2rem] text-center">
              <span>
                <p className="text-[] font-[600]">Turbold</p>
                <p className="font-[200] text-[16px] text-grayLight">
                  Project Manager
                </p>
              </span>
            </div>
          </div>
          <div className="inline-block lg:mx-[2rem] mx-[1rem] mb-[3rem]">
            <StaticImage width={150} src="../images/about/profile.png" alt="" />
            <div className="mt-[2rem] text-center">
              <span>
                <p className="text-[] font-[600]">Turbold</p>
                <p className="font-[200] text-[16px] text-grayLight">
                  Project Manager
                </p>
              </span>
            </div>
          </div>
          <div className="inline-block lg:mx-[2rem] mx-[1rem] mb-[3rem]">
            <StaticImage width={150} src="../images/about/profile.png" alt="" />
            <div className="mt-[2rem] text-center">
              <span>
                <p className="text-[] font-[600]">Turbold</p>
                <p className="font-[200] text-[16px] text-grayLight">
                  Project Manager
                </p>
              </span>
            </div>
          </div>
          <div className="inline-block lg:mx-[2rem] mx-[1rem] mb-[3rem]">
            <StaticImage width={150} src="../images/about/profile.png" alt="" />
            <div className="mt-[2rem] text-center">
              <span>
                <p className="text-[] font-[600]">Turbold</p>
                <p className="font-[200] text-[16px] text-grayLight">
                  Project Manager
                </p>
              </span>
            </div>
          </div>
          <div className="inline-block lg:mx-[2rem] mx-[1rem] mb-[3rem]">
            <StaticImage width={150} src="../images/about/profile.png" alt="" />
            <div className="mt-[2rem] text-center">
              <span>
                <p className="text-[] font-[600]">Turbold</p>
                <p className="font-[200] text-[16px] text-grayLight">
                  Project Manager
                </p>
              </span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
