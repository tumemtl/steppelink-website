import React from "react";
import { StaticImage } from "gatsby-plugin-image";
import HeroBtn from "./Buttons/HeroBtn";

export default function Content() {
  return (
    <div className="flex-row pt-[90px] text-white">
      <div className="z-0 md:visible">
        <StaticImage
          className="!absolute !top-[90px] right-[10%] z-0 w-[100%] opacity-50 md:w-[50%] md:opacity-100 "
          src="../images/robot-1.png"
          placeholder="none"
          alt="Robot"
        />
      </div>
      <div className="relative flex justify-between md:z-10 md:mb-[12%]	">
        <div className="mid:z-40">
          <h2 className=" md:[w-100%] text-center text-[40px] leading-[3rem] md:pt-[4rem] md:text-left md:text-[3.5rem] md:leading-[4.3rem]">
            <span className="font-[200]">Let us make</span>
            <br />
            <span className="">YOUR IMAGINATION</span>
            <br />
            <span className="text-orange">A REALITY</span>
          </h2>
          <p className="text-md w-full pt-[1rem] text-center font-[300] md:w-[45%] md:text-left md:text-[1rem]">
            Formed professional team with the best engineers in Mongolia and
            extensive experience in the field of information technology.
          </p>
          <div className="w-[100%] pt-[2rem] text-center md:text-left">
            <HeroBtn text="Contact Us" onClick={console.log} />
          </div>
        </div>
      </div>
      <div className="mx-auto mt-[2rem] !flex w-[80%] !flex-wrap !justify-center md:z-10 md:w-[100%]">
        <span className="text-center md:inline-block md:align-middle">
          <h1 className="md:mr-[1rem] md:inline-block md:align-middle">
            Trusted by:
          </h1>
          <StaticImage
            placeholder="none"
            width={100}
            className="m-[0.6rem] md:mx-[1.7rem] md:align-middle"
            src="../images/logos/Steppe-arena-1.png"
            alt="Steppe Arena"
          />
          <StaticImage
            placeholder="none"
            width={100}
            className="m-[0.6rem] md:mx-[1.7rem] md:align-middle"
            src="../images/logos/esan-1.png"
            alt="Esan"
          />
          <StaticImage
            placeholder="none"
            width={100}
            className="m-[0.6rem] md:mx-[1.7rem] md:align-middle"
            src="../images/logos/Mesa-2.png"
            alt="MESA"
          />
          <StaticImage
            placeholder="none"
            width={80}
            className="m-[0.6rem] md:mx-[1.7rem] md:align-middle"
            src="../images/logos/MSC-1.png"
            alt="Mongolian Sports Corporation"
          />
          <StaticImage
            placeholder="none"
            width={90}
            className="m-[0.6rem] md:mx-[1.7rem] md:align-middle"
            src="../images/logos/Odin-1.png"
            alt="ODIN"
          />
        </span>
      </div>
    </div>
  );
}
