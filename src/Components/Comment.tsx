import React from "react";
import comment from "../data/commentData.json";
import { useState } from "react";

const changeButton = () => {
  const [count, setCount] = useState(0);

  const decrementCount = () => {
    count !== 0
      ? setCount((prevCount) => prevCount - 1)
      : setCount(comment.length - 1);
  };
  const incrementCount = () => {
    comment.length - 1 !== count
      ? setCount((prevCount) => prevCount + 1)
      : setCount(0);
  };

  return (
    <div className="block">
      <div className="h-[18rem] md:h-[22rem]">
        <h1 className="rotate-180 text-right font-serif text-[100px] font-black leading-[5rem] text-orange">
          ,,
        </h1>
        <p className="text-[16px] leading-[1.6rem] md:text-[24px]">
          {comment[count].text}
        </p>
        <div className="mt-[2rem] text-center text-[16px] md:text-[20px]">
          <span>
            <p className="  font-[600]">{comment[count].user}</p>
            <p className="  text-grey font-[200]">
              {comment[count].userPosition}
            </p>
          </span>
        </div>
      </div>
      <div className="flex justify-end">
        <button
          onClick={decrementCount}
          className="hover:text-greyDark duration-50 relative h-[3.5rem] w-[2.7rem] rounded-tr-lg rounded-bl-lg border-2 border-solid border-orange text-[1.8rem] font-[200] ease-out hover:bg-orange hover:ease-in active:border-orangeDarkDark"
        >
          &#60;
        </button>
        <button
          onClick={incrementCount}
          className="hover:text-greyDark duration-50 relative ml-4 h-[3.5rem] w-[2.7rem] rounded-tl-lg rounded-br-lg border-2 border-solid border-orange text-[1.8rem] font-[200] ease-out hover:bg-orange hover:ease-in active:border-orangeDarkDark"
        >
          &#62;
        </button>
      </div>
    </div>
  );
};

export default changeButton;
