import * as React from "react";

import Cart from "./BlogCart";

const CartContainer: React.FC = () => {
  return (
    <div>
      <Cart />
      <Cart />
      <Cart />
    </div>
  );
};

export default CartContainer;
