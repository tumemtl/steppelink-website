import React from "react";
import { StaticImage } from "gatsby-plugin-image";
import { Link } from "gatsby";
import { FiMoreHorizontal } from "react-icons/fi";
import { MdOutlineBookmarkAdd } from "react-icons/md";
const Cart: React.FC = ({}) => {
  return (
    <div className="mb-[55px] flex border-b-2  pr-[5%] font-roboto md:pr-0">
      <section className="block  ">
        <div className="  text-[13.5px] font-[300] leading-[16px]">
          <div className="flex items-center align-middle [&>*]:mr-[10px]">
            <StaticImage
              src="../../images/about/profile.png"
              width={20}
              alt=""
            />
            <div>
              <p className="">
                Ami Baas
                <span className=" flex-initial items-center px-2 align-middle">
                  .
                </span>
                4 days ago
              </p>
            </div>
          </div>
          <div className="max-h-[115px] overflow-hidden ">
            <Link to="/blogs/blog-slug">
              <h4 className="mb-2 mt-3 text-[21px] font-[400] leading-[24.5px] ">
                Your portfolio is stopping you from geting that job
              </h4>
            </Link>
            <p className="hidden overflow-hidden text-[15.5px] font-[300] leading-[23px] md:block">
              An intense way to learn about the process and practice your
              designs skills — My 1st hackathon Hackathons have been on my mind
              since I heard it was a good way to gain experience as a junior UX
              designer. As my portfolio...
            </p>
          </div>
          <div className="flex justify-between py-[35px] text-[12px]">
            <div className="flex items-center align-middle [&>*]:mr-2">
              <button className="py- px rounded-3xl bg-light py-[4.5px] px-[7.5px] text-grayDark">
                Portfolio
              </button>
              <p className="">3 min read</p>
              <p className="inline-block text-[#757575]">.</p>

              <p> Selected for you</p>
            </div>
            <div className="flex items-center align-middle [&>*]:mx-2">
              <MdOutlineBookmarkAdd className="text-grayLight " size={30} />
              <FiMoreHorizontal size={30} />
            </div>
          </div>
        </div>
      </section>
      <section className="">
        <div className="!relative w-[80px] pb-[85px]  md:w-[135px]">
          <StaticImage
            // src="../../images/services/web.png"
            src="../../images/blogs/blog.png"
            alt=""
            className="!absolute top-0 translate-x-[30%]"
            aspectRatio={1}
          />
        </div>
      </section>
    </div>
  );
};

export default Cart;
