import * as React from "react";
import { StaticImage } from "gatsby-plugin-image";
import Cart from "./Carts/Cart";

const Services: React.FC = () => {
  const paragraph: string = `We will help define exactly what your organization wants through
  its website. It can also be a structured web application with a
  fully dynamic Content Management System (CMS) that can be used by
  anyone with any level of IT knowledge.`;
  return (
    <div className="mt-[100px]">
      <div className="mb-[45px]">
        <h2>Services</h2>
      </div>
      <div className="block gap-5 md:grid md:grid-cols-2">
        <Cart
        //   text={paragraph}
        //   imageUrl="../images/services/web.png"
        //   translateValue={100}
        />
      </div>
    </div>
  );
};

export default Services;
