import { StaticImage } from "gatsby-plugin-image";
import React from "react";

export default function OtherCart() {
  return (
    <div>
      <div className="mb-[2rem] flex h-[20rem] w-full border-l-[10px] border-orange bg-gray">
        <StaticImage
          className="basis-[60%]"
          src="../../images/services/other/Pictre-4.png"
          alt=""
        />
        <div className="basis-[40%] p-[2rem]">
          <h2 className="mb-[2rem] w-[90%] text-[30px] leading-[2rem]">
            System administration
          </h2>
          <p>
            Developed for all types of smartphones (iPhone, Android, Windows),
            mobile devic es (iOS, android, windows) for easy access to
            information and payments.
          </p>
        </div>
      </div>
      <div className="mb-[2rem] flex h-[20rem] w-full border-l-[10px] border-orange bg-gray">
        <StaticImage
          className="basis-[60%]"
          src="../../images/services/other/Pictre-1.png"
          alt=""
        />
        <div className="basis-[40%] p-[2rem]">
          <h2 className="mb-[2rem] w-[90%] text-[30px] leading-[2rem]">
            Consultation
          </h2>
          <p>
            Developed for all types of smartphones (iPhone, Android, Windows),
            mobile devic es (iOS, android, windows) for easy access to
            information and payments.
          </p>
        </div>
      </div>
      <div className="mb-[2rem] flex h-[20rem] w-full border-l-[10px] border-orange bg-gray">
        <StaticImage
          className="basis-[60%]"
          src="../../images/services/other/Pictre-2.png"
          alt=""
        />
        <div className="basis-[40%] p-[2rem]">
          <h2 className="mb-[2rem] w-[90%] text-[30px] leading-[2rem]">
            Hardware research
          </h2>
          <p>
            Developed for all types of smartphones (iPhone, Android, Windows),
            mobile devic es (iOS, android, windows) for easy access to
            information and payments.
          </p>
        </div>
      </div>
      <div className="mb-[2rem] flex h-[20rem] w-full border-l-[10px] border-orange bg-gray">
        <StaticImage
          className="basis-[60%]"
          src="../../images/services/other/Pictre-3.png"
          alt=""
        />
        <div className="basis-[40%] p-[2rem]">
          <h2 className="mb-[2rem] w-[90%] text-[30px] leading-[2rem]">
            Some other thing
          </h2>
          <p>
            Developed for all types of smartphones (iPhone, Android, Windows),
            mobile devic es (iOS, android, windows) for easy access to
            information and payments.
          </p>
        </div>
      </div>
    </div>
  );
}
