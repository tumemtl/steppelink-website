import React from "react";
import { StaticImage } from "gatsby-plugin-image";

interface Props {
  title: string;
  text: string;
}
function SystemDevCart(props: Props) {
  return (
    <section className="mx-auto">
      <div className="h-[597px] max-w-[530px] rounded-r-2xl border-l-8 border-orange bg-gray pl-[3%] pr-[5%] pt-[5%]">
        <div className="min-w-[100px]">
          <StaticImage
            width={100}
            aspectRatio={1}
            src="../../images/icons/desktop.png"
            alt="icon"
          />
        </div>
        <div>
          <h4 className=" mt-[10%] max-w-fit border-b-4 border-orange  pb-9 text-[28px] font-[700]">
            Desktop application
          </h4>
          <p className="pt-9 text-[21px] font-[300] leading-[24px]">
            We will help define exactly what your organization wants through its
            website. It can also be a structured web application with a fully
            dynamic Content Management System (CMS) that can be used by anyone
            with any level of IT knowledge.
          </p>
        </div>
      </div>
    </section>
  );
}

export default SystemDevCart;
