import React from "react";
import Comments from "./Comment";
import { StaticImage } from "gatsby-plugin-image";

export default function Content() {
  return (
    <div className="!relative z-0 mb-[4rem] flex text-white md:static">
      <div className="opacity-50  md:opacity-100">
        <StaticImage
          placeholder="none"
          className="!absolute left-0 z-0 -mt-[25%] w-[] md:w-[60%]"
          src="../images/Yellow-Ellipse-1.png"
          alt="Yellow ellipse"
        />
        <span className="">
          <StaticImage
            placeholder="none"
            className="!absolute left-[15%] z-0 mt-[30%] w-[50%] md:left-[10%] md:mt-[10%] md:w-[30%]"
            src="../images/ellipse-1.png"
            alt="White ellipse"
          />
          <StaticImage
            placeholder="none"
            className="!absolute left-0 z-0 mt-[5rem] md:mt-0 md:w-[60%]"
            src="../images/Robot-2.png"
            alt="Robot 2"
          />
        </span>
      </div>
      <div className="relative z-10 mt-[8%] md:ml-[45%]">
        <div>
          <h2 className="mb-[10%] text-center text-[40px] leading-[3rem] md:w-[100%] md:text-left md:text-[55px] md:leading-[4rem]">
            Only the
            <span className="text-orange"> best and latest </span>
            technology
          </h2>
        </div>
        <div className="!md:inline-block mb-[4rem] w-[100%] !flex-wrap">
          <span className="!flex-wrap justify-between md:inline-block md:align-middle">
            <StaticImage
              width={150}
              placeholder="none"
              className="my-[0.8rem] mx-[1rem] md:mr-[1.8rem] md:align-middle"
              src="../images/languages/Node-Js.png"
              alt="Node JS"
            />
            <StaticImage
              width={40}
              placeholder="none"
              className="my-[0.8rem] mx-[1rem] md:mr-[1.8rem] md:align-middle"
              src="../images/languages/c++.png"
              alt="C++"
            />
            <StaticImage
              width={150}
              placeholder="none"
              className="my-[0.8rem] mx-[1rem] md:mr-[1.8rem] md:align-middle"
              src="../images/languages/Python_logo_and_wordmark.png"
              alt="Python"
            />
            <StaticImage
              width={80}
              placeholder="none"
              className="my-[0.8rem] mx-[1rem] md:mr-[1.8rem] md:align-middle"
              src="../images/languages/Sql_data_base_with_logo.png"
              alt="SQL"
            />
            <StaticImage
              width={150}
              placeholder="none"
              className="my-[0.8rem] mx-[1rem] md:mr-[1.8rem] md:align-middle"
              src="../images/languages/Sass.png"
              alt="Sass"
            />
            <StaticImage
              width={80}
              placeholder="none"
              className="my-[0.8rem] mx-[1rem] md:mr-[1.8rem] md:align-middle"
              src="../images/languages/nextJS.png"
              alt="NextJS"
            />
            <StaticImage
              width={140}
              placeholder="none"
              className="my-[0.8rem] mx-[1rem] md:mr-[1.8rem] md:align-middle"
              src="../images/languages/angular-1.png"
              alt="Angular"
            />
            <StaticImage
              width={150}
              placeholder="none"
              className="my-[0.8rem] mx-[1rem] md:mr-[1.8rem] md:align-middle"
              src="../images/languages/GraphQL.png"
              alt="GraphQL"
            />
            <StaticImage
              width={100}
              placeholder="none"
              className="my-[0.8rem] mx-[1rem] md:mr-[1.8rem] md:align-middle"
              src="../images/languages/images.png"
              alt="UX UI"
            />
          </span>
        </div>
        <div>
          <h2 className="mb-[8%] text-center text-[40px] leading-[3rem] md:w-[100%] md:text-left md:text-[50px] md:leading-[4rem]">
            What
            <span className="text-orange"> our clients </span>
            say about us
          </h2>
          <div>
            <Comments />
          </div>
        </div>
      </div>
    </div>
  );
}
