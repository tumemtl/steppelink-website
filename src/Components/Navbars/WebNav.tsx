import React from "react";
import { Link } from "gatsby";

export default function WebNav() {
  return (
    <div className="my-[2rem] flex flex-wrap justify-center md:my-0 md:block">
      <div className="text-center md:w-[13rem] md:text-left md:text-[24px]">
        <Link
          to=""
          activeClassName="bg-orange border-[2px]"
          className="block rounded-tl-lg border-[2px] border-orange px-[1rem] py-[5px] hover:border-white hover:bg-orange md:border-transparent md:px-0 md:pl-[2rem]"
        >
          <p className="text-center text-[16px] md:text-left md:text-[24px]">
            Technology
          </p>
        </Link>
      </div>
      <div className="text-center md:w-[13rem] md:text-left md:text-[24px]">
        <Link
          to="/"
          activeClassName="bg-orange"
          className="block border-[2px] border-l-[1px] border-orange px-[1rem] py-[5px] hover:border-white hover:bg-orange md:border-transparent md:px-0 md:pl-[2rem]"
        >
          <p className="text-center text-[16px] md:text-left md:text-[24px]">
            Media
          </p>
        </Link>
      </div>
      <div className="text-center md:w-[13rem] md:text-left md:text-[24px]">
        <Link
          to="/"
          activeClassName="bg-orange"
          className="block border-[2px]  border-l-[1px] border-orange py-[5px] hover:border-white hover:bg-orange md:border-transparent md:px-0 md:pl-[2rem]"
        >
          <p className="text-center text-[16px] md:text-left md:text-[24px]">
            Online Shop
          </p>
        </Link>
      </div>
      <div className="text-center md:w-[13rem] md:text-left md:text-[24px]">
        <Link
          to="/"
          activeClassName="bg-orange"
          className="md0 block border-l-[1px] border-theme px-[1rem] py-[5px] hover:border-white hover:bg-orange md:border-[2px] md:border-transparent md:pl-[2rem]"
        >
          <p className="text-center text-[16px] md:text-left md:text-[24px]">
            Pharmacy
          </p>
        </Link>
      </div>
      <div className="text-center md:w-[13rem] md:text-left md:text-[24px]">
        <Link
          to="/"
          activeClassName="bg-orange"
          className="block  border-l-[1px] border-orange py-[5px] hover:border-white hover:bg-orange md:border-transparent md:px-0 md:pl-[2rem]"
        >
          <p className="text-center text-[16px] md:text-left md:text-[24px]">
            Fashion
          </p>
        </Link>
      </div>
      <div className="text-center md:w-[13rem] md:text-left md:text-[24px]">
        <Link
          to="/"
          activeClassName="bg-orange"
          className="block border-[2px] border-l-[1px] border-orange px-[1rem] py-[5px] hover:border-white hover:bg-orange md:border-transparent md:px-0 md:pl-[2rem]"
        >
          <p className="text-center text-[16px] md:text-left md:text-[24px]">
            Music
          </p>
        </Link>
      </div>
      <div className="text-center md:w-[13rem] md:text-left md:text-[24px]">
        <Link
          to="/"
          activeClassName="bg-orange"
          className="block border-[2px] border-l-[1px] border-orange px-[1rem] py-[5px] hover:border-white hover:bg-orange md:border-transparent md:px-0 md:pl-[2rem]"
        >
          <p className="text-center text-[16px] md:text-left md:text-[24px]">
            Books
          </p>
        </Link>
      </div>
      <div className="text-center md:w-[13rem] md:text-left md:text-[24px]">
        <Link
          to="/"
          activeClassName="bg-orange"
          className="block border-[2px] border-l-[1px] border-orange px-[1rem] py-[5px] hover:border-white hover:bg-orange md:border-transparent md:px-0 md:pl-[2rem]"
        >
          <p className="text-center text-[16px] md:text-left md:text-[24px]">
            Arena
          </p>
        </Link>
      </div>
      <div className="text-center md:w-[13rem] md:text-left md:text-[24px]">
        <Link
          to="/"
          activeClassName="bg-orange"
          className="block  border-[2px] border-l-[1px] border-orange px-[1rem] py-[5px] hover:border-white hover:bg-orange md:border-transparent md:px-0 md:pl-[2rem]"
        >
          <p className="text-center text-[16px] md:text-left md:text-[24px]">
            Sports
          </p>
        </Link>
      </div>
    </div>
  );
}
