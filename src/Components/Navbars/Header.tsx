import React from "react";
import { Link } from "gatsby";
import { StaticImage } from "gatsby-plugin-image";
export default function Header() {
  return (
    <div className="relative  ">
      <header className="fixed z-40 flex h-[95px] w-screen justify-between  bg-darkBlue px-[10%] text-white">
        <div className="flex items-center">
          <Link to="/">
            <StaticImage
              src="../../images/logo.png"
              alt="SteppeLink Logo"
              placeholder="tracedSVG"
              // className="mt-[30px]"
            />
          </Link>
        </div>
        <div className="flex  ">
          <nav className="mt-[44px] mb-[35px]">
            <ul className="hidden justify-between text-[13px] md:flex">
              <Link
                to="/about/"
                className="relative block pb-[0.1rem] before:absolute before:right-0 before:bottom-0 before:h-[2px] before:w-[0%] before:bg-orange before:transition-width hover:before:left-0 hover:before:w-[100%] md:mr-[40px]"
                activeClassName="md:mr-[40px] inline-block pb-[0.1rem] relative before:absolute before:bg-orange before:h-[2px] before:w-[100%] before:right-0 before:bottom-0 hover:before:w-[100%] before:transition-width hover:before:left-0"
              >
                About us
              </Link>

              <div className="hover-visible relative">
                <Link
                  to="/our-service"
                  activeClassName="md:mr-[40px] inline-block pb-[0.1rem] relative before:absolute before:bg-orange before:h-[2px] before:w-[100%] before:right-0 before:bottom-0 hover:before:w-[100%] before:transition-width hover:before:left-0"
                  partiallyActive={true}
                  className="relative inline-block pb-[0.1rem] before:absolute before:right-0 before:bottom-0 before:h-[2px] before:w-[0%] before:bg-orange before:transition-width hover:before:left-0 hover:before:w-[100%] md:mr-[40px]"
                >
                  Our service
                </Link>
                <div className="invisible absolute top-[1.5rem] left-0 bg-gray hover:visible">
                  <ul className="">
                    <li className="font-[600]">Services</li>
                    <li>
                      <Link className="font-[300]" to="">
                        System
                      </Link>
                    </li>
                  </ul>
                </div>
              </div>

              {/* <Link
                to="/our-services"
                className="relative block pb-[0.1rem] before:absolute before:right-0 before:bottom-0 before:h-[2px] before:w-[0%] before:bg-orange before:transition-width hover:before:left-0 hover:before:w-[100%] md:mr-[40px]"
                activeClassName="md:mr-[40px] inline-block pb-[0.1rem] relative before:absolute before:bg-orange before:h-[2px] before:w-[100%] before:right-0 before:bottom-0 hover:before:w-[100%] before:transition-width hover:before:left-0"
                >
                Our service
              </Link> */}
              <Link
                className="relative block pb-[0.1rem] before:absolute before:right-0 before:bottom-0 before:h-[2px] before:w-[0%] before:bg-orange before:transition-width hover:before:left-0 hover:before:w-[100%] md:mr-[40px]"
                to="/join-us"
                activeClassName="md:mr-[40px] inline-block pb-[0.1rem] relative before:absolute before:bg-orange before:h-[2px] before:w-[100%] before:right-0 before:bottom-0 hover:before:w-[100%] before:transition-width hover:before:left-0"
              >
                Join Us
              </Link>
              <Link
                className="relative block pb-[0.1rem] before:absolute before:right-0 before:bottom-0 before:h-[2px] before:w-[0%] before:bg-orange before:transition-width hover:before:left-0 hover:before:w-[100%] md:mr-[40px]"
                to="/blogs"
                activeClassName="md:mr-[40px] inline-block pb-[0.1rem] relative before:absolute before:bg-orange before:h-[2px] before:w-[100%] before:right-0 before:bottom-0 hover:before:w-[100%] before:transition-width hover:before:left-0"
              >
                Blog
              </Link>
              <Link
                className="relative block pb-[0.1rem] before:absolute before:right-0 before:bottom-0 before:h-[2px] before:w-[0%] before:bg-orange before:transition-width hover:before:left-0 hover:before:w-[100%] md:mr-[40px]"
                to="/contact-us/"
                activeClassName="md:mr-[40px] inline-block pb-[0.1rem] relative before:absolute before:bg-orange before:h-[2px] before:w-[100%] before:right-0 before:bottom-0 hover:before:w-[100%] before:transition-width hover:before:left-0"
              >
                Contact Us
              </Link>
            </ul>
          </nav>
          <div className="mt-[32px]">
            <button className=" h-[auto] w-[auto] rounded-full bg-orange py-[10px] px-[18px] text-[14px] font-[700] uppercase leading-[17px]">
              G<span className="text-[12px] leading-[14px]">et in Touch</span>
            </button>
          </div>
        </div>
      </header>
    </div>
  );
}
