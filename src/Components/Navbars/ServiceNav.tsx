import React from "react";
import { Link } from "gatsby";

export default function ServiceNav() {
  return (
    <div className="flex flex-col justify-between border-b-[2px] border-orange font-[200] md:flex-row">
      <Link
        to="/our-service"
        activeClassName="bg-orange"
        className="w-full rounded-tl-xl border-b-[1px] border-orange py-[1%] text-center hover:bg-orange md:border-0"
      >
        System Development
      </Link>
      <Link
        to="/our-service/mobile-dev"
        activeClassName="bg-orange"
        className="w-full border-b-[1px] border-orange py-[1%] text-center hover:bg-orange md:border-0"
      >
        Mobile Development
      </Link>
      <Link
        to="/our-service/web-dev"
        activeClassName="bg-orange"
        className="w-full border-b-[1px] border-orange py-[1%] text-center hover:bg-orange md:border-0"
      >
        Web Development
      </Link>
      <Link
        to="/our-service/system-dev"
        activeClassName="bg-orange"
        className="w-full border-b-[1px]  border-orange py-[1%] text-center hover:bg-orange md:border-0"
      >
        AI Development
      </Link>
      <Link
        to="/our-service/other"
        activeClassName="bg-orange"
        className="w-full rounded-tr-xl py-[1%] text-center hover:bg-orange"
      >
        Other
      </Link>
    </div>
  );
}
