import { StaticImage } from "gatsby-plugin-image";
import React from "react";
import Abilities from "./Abilities";
import Teammates from "./Carts/Teammates";

export default function Timeline() {
  return (
    <div className="pt-[4rem]">
      <h2 className="relative z-20 text-center leading-[4rem] md:text-left">
        Steppelink <br /> <span className="text-orange">timeline</span>
      </h2>
      <span className="absolute right-0 top-[3rem] z-0">
        <StaticImage
          src="../images/about/robot.png"
          placeholder="none"
          alt=""
        />
      </span>
      <div className="relative z-20 h-[40rem] bg-orange opacity-20"></div>

      <div className="mb-[8rem]">
        <h2 className="mb-[1rem] text-center md:mb-[2rem]">Our Misson</h2>
        <p className="mx-auto mb-[5rem] w-[80%] text-center text-[18px] md:text-[24px]">
          {" "}
          We rely heavily on our Steppelink LLC’s Steppe Payment. In the past we
          had difficult control over our customers’ transactions, but now our
          admin is able to access every information related from one place.
        </p>
        <h2 className="mb-[1rem] text-center md:mb-[2rem]">Our Goal</h2>
        <p className="mx-auto w-[80%] text-center text-[18px] md:text-[24px]">
          {" "}
          We rely heavily on our Steppelink LLC’s Steppe Payment. In the past we
          had difficult control over our customers’ transactions, but now our
          admin is able to access every information related from one place.
        </p>
      </div>

      <Teammates />

      <Abilities />
    </div>
  );
}
