import React from "react";
import { FaAngleRight } from "react-icons/fa";

export default function ContactUsBtn() {
  return (
    <a href="/contact-us" className="group">
      <button className="duration-50 mx-auto my-[4rem] flex h-[8rem] w-[70%] items-center justify-between bg-orange font-[400] text-white ease-out hover:bg-orangeDark hover:ease-in active:bg-orangeDarkDark">
        <p className="ml-[4rem] text-[1.8rem]">Let's get talking</p>
        <div className="flex items-center">
          <div className="relative mr-[4rem]  max-w-max">
            <p className="relative z-10 px-[1.5rem] text-center text-[20px]">
              Contact us
            </p>
            <div className="absolute bottom-0 top-0 z-0 my-auto h-[3.5rem] w-[3.5rem] rounded-full bg-theme pl-[2rem] transition-width group-hover:w-full"></div>
          </div>
          <div className="flex h-[8rem] w-[3rem] items-center justify-center bg-white ">
            <FaAngleRight size={24} className="text-orange" />
          </div>
        </div>
      </button>
    </a>
  );
}
