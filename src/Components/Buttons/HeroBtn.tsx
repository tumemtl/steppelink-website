import { ButtonConfig } from "../../interfaces";
import React from "react";

const HeroBtn = ({ text, onClick }: ButtonConfig) => {
  return (
    <button
      className="duration-50 w-[207.36px] rounded-full bg-orange py-[0.8rem] px-10 text-[1.20rem] font-[200] ease-out hover:bg-orangeDark hover:ease-in active:bg-orangeDarkDark"
      onClick={onClick}
    >
      {text}
    </button>
  );
};

export default HeroBtn;
