import React from "react";
import { IoLinkSharp } from "react-icons/io5";

export default function ShareBtn() {
  return (
    <button className="mr-[2rem] flex h-[35px] w-[8rem] items-center justify-center">
      <span className="align-center mr-[10px] flex justify-center">
        <IoLinkSharp className="" />
      </span>
      Share
    </button>
  );
}
