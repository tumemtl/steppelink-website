import React from "react";

export default function ApplyBtn() {
  return (
    <button className="placeholder:orangeDarkDark flex h-[35px] w-[8rem] items-center justify-center rounded-tl-lg rounded-br-lg border-[2px] border-white bg-orange">
      Apply
    </button>
  );
}
