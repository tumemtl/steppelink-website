import React from "react";
import { StaticImage } from "gatsby-plugin-image";
import { Link } from "gatsby";

import {
  FaLinkedin,
  FaFacebookSquare,
  FaTwitterSquare,
  FaLinkedinIn,
} from "react-icons/fa";

export default function Content() {
  return (
    <>
      <div className="justify-between border-b-[1px] border-white md:flex md:p-[4rem]">
        <ul className="block list-none">
          <li className="border-white">
            <Link to="./">
              <StaticImage
                quality={100}
                width={100}
                src="../images/logo.png"
                alt="SteppeLink Logo"
                placeholder="tracedSVG"
              />
            </Link>
          </li>
          <li>
            <a href="./" className="font-[600]">
              Lets work together
            </a>
          </li>
        </ul>
        <ul className="list-none flex-col md:flex">
          <li>
            <a
              className="relative mt-[0.5rem] inline-block  border-b-[1px] before:absolute before:right-0 before:bottom-0 before:h-[2px] before:w-[0%] before:bg-orange before:transition-width hover:before:left-0 hover:before:w-[100%] md:my-[1rem] md:border-0"
              href="/about/"
            >
              About Us
            </a>
          </li>
          <li>
            <a
              className="relative my-[0.5rem] inline-block border-b-[1px]  before:absolute before:right-0 before:bottom-0 before:h-[2px] before:w-[0%] before:bg-orange before:transition-width hover:before:left-0 hover:before:w-[100%] md:my-[0] md:mb-[1rem] md:border-0"
              href="/our-service"
            >
              Our Services
            </a>
          </li>
          <li>
            <a
              className="relative  inline-block border-b-[1px] pb-[0] before:absolute before:right-0 before:bottom-0 before:h-[2px] before:w-[0%] before:bg-orange before:transition-width hover:before:left-0 hover:before:w-[100%] md:border-0"
              href="/join-us"
            >
              Join Us
            </a>
          </li>
        </ul>
        <ul className="list-none">
          <li>
            <a
              className="relative my-[0.5rem] mt-0 inline-block border-b-[1px]  before:absolute before:right-0 before:bottom-0 before:h-[2px] before:w-[0%] before:bg-orange before:transition-width hover:before:left-0 hover:before:w-[100%] md:mb-[1rem] md:mt-[1rem] md:border-0"
              href="tel:98989898"
            >
              +976 98989898
            </a>
          </li>

          <li>
            <a
              className="relative inline-block before:absolute before:right-0 before:bottom-0 before:h-[2px] before:w-[0%] before:bg-orange before:transition-width hover:before:left-0 hover:before:w-[100%] md:mb-[1rem]"
              href="mailto:info@steppelink.mn"
            >
              info@steppelink.mn
            </a>
          </li>
        </ul>
        <div className="relative hidden max-h-[2rem] w-auto overflow-hidden md:block">
          <button className="duration-50 h-[2rem] w-auto overflow-hidden rounded-br-full rounded-bl-full rounded-tl-full border-[2px] border-orange bg-theme px-[10px] pr-[40px] text-left text-[1.20rem] font-[400] ease-out hover:bg-orangeDark hover:ease-in active:bg-orangeDarkDark">
            Get in touch
          </button>
          <span className="absolute right-0 top-[50%] my-auto h-full min-h-[1rem] w-[30px] -translate-y-[50%] overflow-hidden rounded-br-lg bg-orange pt-1 pl-[10px]">
            &#62;
          </span>
        </div>
      </div>
      <div className="my-[1rem] flex justify-between">
        <small className="">© All rights reserved 2022 Steppe Link LLC</small>
        <div className="flex w-[6rem] justify-between">
          <Link to="https://www.linkedin.com/company/steppelink-holding/">
            <FaLinkedin className="hover:fill-orange" size={24} />
          </Link>
          <Link to="https://www.facebook.com/Steppelinkholding/">
            <FaFacebookSquare className="hover:fill-orange" size={24} />
          </Link>
          <Link to="">
            <FaTwitterSquare className="hover:fill-orange" size={24} />
          </Link>
        </div>
      </div>
    </>
  );
}
