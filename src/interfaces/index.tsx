import React, { ReactNode } from "react";

export type ButtonConfig = {
  text: ReactNode;
  // fn?: Function;
  onClick: () => void;
};

export type Comment = {
  text: ReactNode;
  user: ReactNode;
  userPosition: ReactNode;
};
